package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;
    private int jumlahpenilaian;
    private String tustring;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama=nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot=bobot;
        this.jumlahpenilaian=banyakButirPenilaian;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        this.butirPenilaian[idx]=butir;
        this.tustring = butir.toString();
    }

    public String getNama() {
        return this.nama;
    }

    public double getRerata() {
        double counter=0.0;
        int num=0;
        for (int n = 0; n<this.butirPenilaian.length; n++){
            if (this.butirPenilaian[n]!=null){
                counter += this.butirPenilaian[n].getNilai();
                num+=1;

            }
        }
        double rerata;
        if (num!=0){rerata = counter/num;}
        else{ rerata = 0;}
        return rerata;
    }

    public double getNilai() {
        double rerata = getRerata();
        return rerata*this.bobot/100;
    }

    public String getDetail() {
        String hasil = ""+"~~~("+this.nama+this.bobot+"%) ~~~";
        String hasil1 = "";
        for (int i = 1; i<=this.jumlahpenilaian;i++){
            hasil1+=this.nama+" "+i+" :"+this.tustring+"\n";
        }
        return hasil+hasil1;
    }

    @Override
    public String toString() {
        String angka = String.format("%.02f", getRerata());
        return ""+"Rerata "+this.nama+": "+angka;
    }

}
