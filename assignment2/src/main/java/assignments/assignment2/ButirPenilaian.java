package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    public double getNilai() {
        if (this.nilai-(PENALTI_KETERLAMBATAN/100.0)*this.nilai<0){
            this.nilai = 0;
        }
        else {
            if(terlambat){
                return this.nilai-(PENALTI_KETERLAMBATAN/100.0)*this.nilai;
            }
            
        }
    return this.nilai;
    }


    @Override
    public String toString() {
        String angka = String.format("%.02f", this.getNilai());
        if (this.terlambat==true){
            return ""+angka+" (T)";
        }
        else{
            return ""+angka;
        }
    }
}
