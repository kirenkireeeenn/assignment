package assignments.assignment2;

import java.util.*;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode=kode;
        this.nama=nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        for (int c =0; c<this.mahasiswa.size(); c++){
            if (this.mahasiswa.get(c).getNpm().equals(npm)){
                return this.mahasiswa.get(c);}
                  }
                  return null;
        
    }

    public String rekap() {
        String hasil="";
        for (int c =0; c<this.mahasiswa.size(); c++){
            hasil+=this.mahasiswa.get(c).toString()+"\n"+this.mahasiswa.get(c).rekap()+"\n";
                  }
        return hasil;
    }

    public String toString() {
        return this.kode+" - "+this.nama;
    }
}
