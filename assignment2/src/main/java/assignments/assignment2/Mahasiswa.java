package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm=npm;
        this.nama=nama;
        this.komponenPenilaian=komponenPenilaian;
    }
    
    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (int f = 0; f<this.komponenPenilaian.length;f++) {
            if (komponenPenilaian[f].getNama().equals(namaKomponen)) {
                return komponenPenilaian[f];
            }
            else {continue;}
        }
        return null;
    }

    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        String hasil="";
        double total=0.0;
        for (int x = 0; x<komponenPenilaian.length;x++){
            hasil+=""+"Rerata "+komponenPenilaian[x]+": "+komponenPenilaian[x].getRerata()+"\n";
            total+=komponenPenilaian[x].getNilai();
        }
        hasil+=""+"Nilai Akhir : "+total+"\n"+"Huruf : "+getHuruf(total)+"\n"+getKelulusan(total);
        return hasil;
    }

    public String toString() {
        return ""+this.npm+" - "+this.nama;
    }

    public String getDetail() {
        String hasil = "";
        for (int x = 0; x<komponenPenilaian.length;x++){
            hasil+=komponenPenilaian[x].getDetail();
        }
        return hasil+rekap();
    }

    @Override
    public int compareTo(Mahasiswa other) {
        return this.npm.compareTo(other.getNpm());
    }
}
